$(function () {
    "use strict";

    const $findButton = $('#findJob'),
          $closeButton = $('#closeModal'),
          $window = $(window),
          $modal = $('#infoModal');

    $findButton.on('click', function () {
        if($modal.hasClass('hide')) {
            openModal();
            if ($window.width() < 960) {
                scrollTo($modal);
            }
        } else {
            closeModal();
        }
    });

    $closeButton.on('click', function () {
        closeModal();
    });

    function closeModal() {
        $findButton.fadeIn('slow');
        $modal.fadeOut('fast');
    }

    function openModal() {
        $findButton.fadeOut('fast');
        $modal.fadeIn('slow');
    }

    function scrollTo(element) {
        $('html, body').animate({
            scrollTop: element.offset().top
        }, 1000);
    }

});